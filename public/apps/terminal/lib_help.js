/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_help_general(term) {
	term.writeln('  clear                       : Clear terminal window.');
	term.writeln('  exit                        : Close terminal window.');
	term.writeln('  help [fs|fun|net|ots|sys]   : Show this help or section.');
}

function terminal_help_filesystem(term) {
	term.writeln('  cat <file> [...]            : Concatenate files and print to output.');
	term.writeln('  cd <directory>              : Change current directory.');
	term.writeln('  cp <source> <destination>   : Copy the source file to the destination.');
	term.writeln('  edit <file>                 : Start editor (Notepad) to edit file.');
	term.writeln('  ls [<file|directory>]       : List file or directory content.');
	term.writeln('  mkdir <directory> [...]     : Create directory.');
	term.writeln('  mv <source> <destination>   : Move the source to the destination.');
	term.writeln('  open <file|directory> [...] : Open file or directory.');
	term.writeln('  rm <file> [...]             : Remove file.');
	term.writeln('  rmdir <directory> [...]     : Remove directory.');
	term.writeln('  search <query>              : Search files matching the query.');
}

function terminal_help_fun(term) {
	term.writeln('  hack [type]                 : Show fake hacking windows.');
	term.writeln('    type: theme               : Change Orb theme temporarily.');
	term.writeln('          restore             : Restore the user\'s Orb theme.');
	term.writeln('          all                 : Show and position all hacking windows.');
	term.writeln('          crack               : Show password cracking window.');
	term.writeln('          decrypt             : Show file decrypter window.');
	term.writeln('          download            : Show file downloader window.');
	term.writeln('          exploit             : Show exploit window.');
	term.writeln('          map                 : Show live hacking map window.');
	term.writeln('          source              : Show source code window.');
	term.writeln('          uplink              : Show satellite uplink window.');
	term.writeln('          close               : Close all hacking windows.');
}

function terminal_help_network(term) {
	term.writeln('  dns <hostname|IP address>   : DNS lookup.');
	term.writeln('  ping <hostname|IP address>  : Ping host');
	term.writeln('  port <host> <port>          : Test whether port is open or closed.');
}

function terminal_help_script(term) {
	term.writeln('  anykey [<text>]             : Show text and wait for a key press.');
	term.writeln('  echo [<text>]               : Show text.');
	term.writeln('  minimize                    : Minimize terminal window.');
}

function terminal_help_system(term) {
	term.writeln('  color #<RRGGBB>             : Change window color.');
	if (typeof javascript_execute_file == 'function') {
		term.writeln('  exec <file>                  : Execute Javascript file.');
	}
	term.writeln('  kill <pid>                  : Terminate application.');
	term.writeln('  ps                          : Show process list.');
	term.writeln('  wallpaper <image file>      : Set wallpaper.');
	term.writeln('  version                     : Print Orb version number.');
}

function terminal_show_help(term, parameters) {
	if (parameters.length == 0) {
		terminal_help_general(term);
	} else parameters.forEach(function(section) {
		switch (section) {
			case 'fs': terminal_help_filesystem(term); break;
			case 'fun': terminal_help_fun(term); break;
			case 'net': terminal_help_network(term); break;
			case 'ots': terminal_help_script(term); break;
			case 'sys': terminal_help_system(term); break;
			default: term.writeln('Unknown section.');
		}
	});

	terminal_done(term);
}
