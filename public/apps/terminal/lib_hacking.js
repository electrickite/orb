/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

var terminal_characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
var terminal_passwords = [ '12345', '123456', '123456789', 'test1', 'password', '12345678', 'zinch', 'g_czechout', 'asdf', 'qwerty', '1234567890', '1234567', 'Aa123456.', 'iloveyou', '1234', 'abc123', '111111', '123123', 'dubsmash', 'test', 'princess', 'qwertyuiop', 'sunshine', 'BvtTest123', '11111', 'ashley', '00000', '000000', 'password1', 'monkey', 'livetest', '55555', 'soccer', 'charlie', 'asdfghjkl', '654321', 'family', 'michael', '123321', 'football', 'baseball', 'q1w2e3r4t5y6', 'nicole', 'jessica', 'purple', 'shadow', 'hannah', 'chocolate', 'michelle', 'daniel', 'maggie', 'qwerty123', 'hello', '112233', 'jordan', 'tigger', '666666', '987654321', 'superman', '12345678910', 'summer', '1q2w3e4r5t', 'fitness', 'bailey', 'zxcvbnm', 'fuckyou', '121212', 'buster', 'butterfly', 'dragon', 'jennifer', 'amanda', 'justin', 'cookie', 'basketball', 'shopping', 'pepper', 'joshua', 'hunter', 'ginger', 'matthew', 'abcd1234', 'taylor', 'samantha', 'whatever', 'andrew', '1qaz2wsx3edc', 'thomas', 'jasmine', 'animoto', 'madison', '0987654321', '54321', 'flower', 'Password', 'maria', 'babygirl', 'lovely', 'sophie', 'Chegg123' ];
var terminal_decrypt_x = 32;
var terminal_decrypt_y = 16;
var terminal_exploit_commands = {
	 0: 'Scanning remote network...',
	 3: 'Server found.',
	 4: 'Querying DNS...',
	 6: 'Starting port scan...',
	10: 'Targeting vulnerable service.',
	11: 'Identifying remote service...',
	13: 'Matching service fingerprint with exploit database.',
	15: 'Uploading exploit.',
	17: 'Starting remote shell',
	18: 'Installing backdoor.',
	20: 'Erasing remote logfiles.',
	23: 'Targeting new network.',
	25: null };
var terminal_downloads = [
	[ 'Backup.tar.gz', 9 ],
	[ 'Management meeting.docx', 4 ],
	[ 'Passwords.xlsx', 7 ],
	[ 'Annual report.pdf', 15 ],
	[ 'Business plans.pptx', 3 ],
	[ 'Product designs.zip', 20 ],
	[ 'Risk analysis.pdf', 8 ],
	[ 'Building floor plans.zip', 14 ],
	[ 'Financial overview.xlsx', 7 ],
	[ 'Prototype.jpg', 4 ] ];

function terminal_hex_number(length = 1, separator = ' ') {
	result = [];

	while (length > 0) {
		result.push(Math.floor(Math.random() * 256).toString(16).padStart(2, '0'));
		length--;
	}

	return result.join(separator);
}

/* Hacking
 */
function terminal_hack(term, parameters) {
	if (parameters.length == 0) {
		term.writeln('Arguments missing.');
		terminal_done(term);
		return;
	}

	parameters.forEach(function(param) {
		switch (param) {
			case 'theme':
				if ($('div.desktop canvas.matrix').length == 0) {
					orb_window_set_color('#206020');
					terminal_matrix_rain();
				}
				break;
			case 'restore':
				if ($('div.desktop canvas.matrix').length > 0) {
					orb_setting_get('system/color', function(color) {
						orb_window_set_color(color);
					});

					var rain_interval = $('div.desktop canvas.matrix').data('interval');
					window.clearInterval(rain_interval);
					$('div.desktop canvas.matrix').remove();
					orb_setting_get('system/wallpaper', function(wallpaper) {
						orb_desktop_load_wallpaper(wallpaper);
					});
				}
				break;
			case 'close':
				$('div.windows > div.window').each(function() {
					if ($(this).find('div.terminal-hacker').length > 0) {
						$(this).close();
					}
				});
				break;
			case 'all':
				terminal_crack();
				terminal_decrypt();
				terminal_download();
				terminal_exploit();
				terminal_map();
				terminal_source();
				terminal_uplink();

				var scr_width = $('div.desktop').outerWidth();
				var scr_height = $('div.desktop').outerHeight() - 36;

				$('div.window').each(function() {
					var width = $(this).outerWidth();
					var height = $(this).outerHeight();

					var title = $(this).find('div.window-header div.title').text();

					switch (title) {
						case 'Hacking monitor':
							$(this).css('top', '25px');
							$(this).css('left', '25px');
							break;
						case 'File decrypter':
							$(this).css('top', '50px');
							$(this).css('left', (scr_width - width - 800) + 'px');
							break;
						case 'Remote exploit':
							$(this).css('top', '150px');
							$(this).css('left', (scr_width - width - 150) + 'px');
							break;
						case 'Password cracker':
							$(this).css('top', '550px');
							$(this).css('left', (scr_width - width - 300) + 'px');
							break;
						case 'Source code':
							$(this).css('top', (scr_height - height - 50) + 'px');
							$(this).css('left', '50px');
							break;
						case 'File downloader':
							$(this).css('top', (scr_height - height - 100) + 'px');
							$(this).css('left', '1000px');
							break;
						case 'Satellite uplink':
							$(this).css('top', (scr_height - height - 25) + 'px');
							$(this).css('left', (scr_width - width - 25) + 'px');
							break;
					}

					console.log(title);
				});
				break;
			case 'crack':
				terminal_crack();
				break;
			case 'decrypt':
				terminal_decrypt();
				break;
			case 'download':
				terminal_download();
				break;
			case 'exploit':
				terminal_exploit();
				break;
			case 'map':
				terminal_map();
				break;
			case 'source':
				terminal_source();
				break;
			case 'uplink':
				terminal_uplink();
				break;
			default:
				term.writeln('Invalid parameter.');
				break;
		}
	});

	terminal_done(term);
}

/* Matrix rain
 */
function terminal_matrix_rain() {
	$('div.desktop').css('background-image', '');
	$('div.desktop').css('background-color', '#000000');

	$('div.desktop').append('<canvas class="matrix"></canvas>');
	var matrix_canvas = $('canvas.matrix')[0];
	matrix_ctx = matrix_canvas.getContext('2d');

	var letters = 'ﾊﾐﾋｰｳｼﾅﾓﾆｻﾜﾂｵﾘｱﾎﾃﾏｹﾒｴｶｷﾑﾕﾗｾﾈｽﾀﾇﾍ'.split('');
	var fontSize = 12;
	var columns;

	var resize = function() {
		var old_columns = columns;
		matrix_canvas.width = window.innerWidth;
		matrix_canvas.height = window.innerHeight - $('div.taskbar').height();
		columns = Math.ceil(matrix_canvas.width / fontSize);
		matrix_ctx.font = Math.round(1.2 * fontSize) + 'px Verdana';

		for (var i = old_columns; i < columns; i++) {
			drops[i] = matrix_canvas.height;
		}
	};

	resize();

	var drops = [];
	for (var i = 0; i < columns; i++) {
		drops[i] = matrix_canvas.height;
	}

	var matrix_draw = function() {
		matrix_ctx.fillStyle = 'rgba(0, 0, 0, .1)';
		matrix_ctx.fillRect(0, 0, matrix_canvas.width, matrix_canvas.height);

		for (var i = 0; i < drops.length; i++) {
			var text = letters[Math.floor(Math.random() * letters.length)];
			matrix_ctx.fillStyle = '#00ff00';
			matrix_ctx.fillText(text, i * fontSize, drops[i] * fontSize);
			drops[i]++;
			if (drops[i] * fontSize > matrix_canvas.height && Math.random() > .95) {
				drops[i] = 0;
			}
		}
	}

	$(window).on('resize', resize);
	var rain_interval = setInterval(matrix_draw, 33);

	$('div.desktop canvas.matrix').data('interval', rain_interval);
}

/* Password cracking
 */
function terminal_crack_interval(crack_window) {
	var counter = crack_window.data('counter');

	if (counter == 0) {
		var hash = '';
		for (var i = 0; i < 20; i++ ) {
			var pos = Math.floor(Math.random() * terminal_characters.length);
			hash += terminal_characters.charAt(pos);
		}
		crack_window.find('input.hash').val(hash);

		var pwd = Math.floor(Math.random() * terminal_passwords.length);
		var password = terminal_passwords[pwd];
		crack_window.find('input.password').val(password);

		counter = 20;
	} else {
		counter--;
	}

	crack_window.data('counter', counter);

	var key = terminal_hex_number(27);
	crack_window.find('textarea.primary').text(key);

	key = terminal_hex_number(18);
	crack_window.find('textarea.secondary').text(key);
}

function terminal_crack() {
	var window_content =
		'<div class="terminal-hacker">' +
		'<h1>Password cracker</h1>' +
		'<table class="crack">' +
		'<tr><td>Current password hash:</td><td><input class="hash" disabled="disabled" /></td></tr>' +
		'<tr><td>Primary key:</td><td><textarea class="primary" disabled="disabled" style="height:70px"></textarea></td></tr>' +
		'<tr><td>Secondary key:</td><td><textarea class="secondary" disabled="disabled" style="height:50px"></textarea></td></tr>' +
		'<tr><td>Cracked password:</td><td><input class="password" disabled="disabled" /></td></tr>' +
		'</table>' +
		'</div>';

	var crack_window = $(window_content).orb_window({
		header: 'Password cracker',
		icon: '/apps/terminal/terminal.png',
		bgcolor: '#000000',
		width: 500,
		height: 250,
		resize: false,
		maximize: false,
		close: function() {
			window.clearInterval(crack_interval);
		}
	});

	crack_window.open();
	crack_window.data('counter', 0);

	var crack_interval = window.setInterval(terminal_crack_interval, 150, crack_window);
}

/* Decrypter
 */
function terminal_decrypt_interval(decrypt_window) {
	var counter = decrypt_window.data('counter');

	if (counter == 0) {
		decrypt_window.find('td').text('.');
		decrypt_window.find('td').removeClass('decrypted');

		counter = 400;
	} else {
		var pos = Math.floor(Math.random() * terminal_characters.length);
		var chr = terminal_characters.charAt(pos);
		var x = Math.floor(Math.random() * terminal_decrypt_x) + 1;
		var y = Math.floor(Math.random() * terminal_decrypt_y) + 1;

		var td = decrypt_window.find('tr:nth-child(' + y + ')').find('td:nth-child(' + x + ')');
		td.text(chr);
		td.addClass('decrypted');

		counter--;
	}

	decrypt_window.data('counter', counter);

}

function terminal_decrypt() {
	var row = '<tr>' + '<td></td>'.repeat(terminal_decrypt_x) + '</tr>';
	var window_content = '<div class="terminal-hacker"><h1>File decrypter</h1><table class="file">';
	window_content += row.repeat(terminal_decrypt_y);
	window_content += '</table></div>';

	var decrypt_window = $(window_content).orb_window({
		header: 'File decrypter',
		icon: '/apps/terminal/terminal.png',
		bgcolor: '#000000',
		width: 340,
		height: 370,
		resize: false,
		maximize: false,
		close: function() {
			window.clearInterval(decrypt_interval);
		}
	});

	decrypt_window.open();
	decrypt_window.data('counter', 0);

	var decrypt_interval = window.setInterval(terminal_decrypt_interval, 50, decrypt_window);
}

/* File downloader
 */
function terminal_download_interval(download_window) {
	var file = download_window.data('file');
	var progress = download_window.data('progress');

	var file_data = terminal_downloads[file];
	var filename = file_data[0];
	var filesize = file_data[1];

	if (progress == 0) {
		var entry = '<div class="download">' +
			'<div class="filename">' + filename + '</div>' +
			'<div class="status"><div class="percentage" style="width:0%"></div></div>' +
			'</div>';
		download_window.find('div.files').append(entry);

		var bottom = download_window.find('div.files').outerHeight();
		download_window[0].scroll(0, bottom);

		download_window.data('progress', progress + 1);
	} else if (progress <= filesize) {
		var entry = download_window.find('div.download').last();
		var perc = Math.round(100 * progress / filesize);
		entry.find('div.percentage').css('width', perc + '%');

		download_window.data('progress', progress + 1);
	} else {
		if (++file == terminal_downloads.length) {
			file = 0;
		}

		download_window.data('file', file);
		download_window.data('progress', 0);
	}
}

function terminal_download() {
	var window_content = '<div class="terminal-hacker"><div class="files"></div></div>';

	var download_window = $(window_content).orb_window({
		header: 'File downloader',
		icon: '/apps/terminal/terminal.png',
		bgcolor: '#000000',
		width: 600,
		height: 150,
		resize: false,
		maximize: false,
		close: function() {
			window.clearInterval(download_interval);
		}
	});

	download_window.open();
	download_window.data('file', 0);
	download_window.data('progress', 0);

	download_window.css('overflow-y', 'scroll');
	download_window.css('width', 'calc(100% + 30px)');
	download_window.css('padding-right', '15px');

	var download_interval = window.setInterval(terminal_download_interval, 250, download_window);
}

/* Exploit
 */
function terminal_exploit_interval(exploit_window) {
	var counter = exploit_window.data('counter');

	var command = terminal_exploit_commands[counter];

	if (command === null) {
		exploit_window.empty();

		counter = 0;
	} else {
		if (command !== undefined) {
			exploit_window.append('<div>&gt; ' + command + '</div>');
		}

		counter++;
	}

	exploit_window.data('counter', counter);
}

function terminal_exploit() {
	var window_content =
		'<div class="terminal-hacker">' +
		'</div>';

	var exploit_window = $(window_content).orb_window({
		header: 'Remote exploit',
		icon: '/apps/terminal/terminal.png',
		bgcolor: '#000000',
		width: 500,
		height: 300,
		resize: false,
		maximize: false,
		close: function() {
			window.clearInterval(exploit_interval);
		}
	});

	exploit_window.open();
	exploit_window.data('counter', 0);

	var exploit_interval = window.setInterval(terminal_exploit_interval, 1000, exploit_window);
}

/* Hacking map
 */
function terminal_map() {
	var window_content =
		'<div class="terminal-hacker">' +
		'<iframe src="https://threatmap.bitdefender.com/"></iframe>' +
		'</div>';

	var map_window = $(window_content).orb_window({
		header: 'Hacking monitor',
		icon: '/apps/terminal/terminal.png',
		bgcolor: '#000000',
		width: 800,
		height: 550
	});

	map_window.open();
}

/* Source code listing
 */
function terminal_source_interval(source_window) {
	var source = source_window.find('div.source');
	var scroll = Math.round(source.scrollTop());

	scroll += 4;

	source[0].scroll(0, scroll);

	if (Math.round(source.scrollTop()) != scroll) {
		source[0].scroll(0, 0);
	}
}

function terminal_source() {
	var window_content =
		'<div class="terminal-hacker">' +
		'<div class="source"></div>' +
		'</div>';

	var source_window = $(window_content).orb_window({
		header: 'Source code',
		icon: '/apps/terminal/terminal.png',
		bgcolor: '#000000',
		width: 800,
		height: 400,
		maximize: false,
		close: function() {
			window.clearInterval(source_interval);
		}
	});

	$.ajax({
		url: '/terminal/source'
	}).done(function(data) {
		var source = $(data).find('source').text();
		source = source.replace(/\t/g, '    ');
		source_window.find('div.source').text(source);
	});

	var source_interval = window.setInterval(terminal_source_interval, 25, source_window);

	source_window.open();
}

/* Satellite uplink
 */
function terminal_uplink() {
	var window_content =
		'<div class="terminal-hacker">' +
		'<h1>Satellite uplink</h1>' +
		'<img src="/apps/terminal/hacker-uplink.gif" draggable="false" />' +
		'</div>';

	var uplink_window = $(window_content).orb_window({
		header: 'Satellite uplink',
		icon: '/apps/terminal/terminal.png',
		bgcolor: '#000000',
		width: 600,
		height: 400,
		resize: false,
		maximize: false
	});

	uplink_window.open();
}
