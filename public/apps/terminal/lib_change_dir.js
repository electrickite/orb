/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_change_dir(term, directory) {
	if (directory.length > 1) {
		term.writeln('Too many arguments.');
		terminal_done(term);
		return;
	}

	if (directory.length == 0) {
		directory = '/';
	} else {
		directory = directory[0];

		while (directory.substr(-1) == '/') {
			directory = directory.substr(0, directory.length - 1);
		}

		if (directory == '.') {
			terminal_done(term);
			return;
		} else if (directory == '') {
			directory = '/';
		}
	}

	directory = terminal_combine_directories(term.path, directory);

	orb_directory_exists(directory, function(exists) {
		if (exists) {
			term.path = directory;
		} else {
			term.writeln('Directory not found.');
		}
		terminal_done(term);
	}, function() {
		terminal_done(term);
	});

	return true;
}
