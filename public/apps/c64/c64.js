/* Orb C64 application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

var c64_valid_extensions = [ 'd64', 'prg', 's64', 't64' ];

function c64_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {	
		return false;
	}

	return c64_valid_extensions.includes(extension);
}

function c64_open(filename = undefined) {
	var window_content =
		'<div class="c64">' +
		'<iframe id="c64" src="/apps/c64/c64.php"></iframe>' +
		'<div class="btn-group keys">' +
		'<button key="112">F1</button>' +
		'<button key="114">F3</button>' +
		'<button key="116">F5</button>' +
		'<button key="118">F7</button>' +
		'<button key="27">Run/Stop</button>' +
		'<button key="9">Control</button>' +
		'<button key="172">C=</button>' +
		'</div></div>';

	var c64_window = $(window_content).orb_window({
		header:'C64',
		icon: '/apps/c64/c64.png',
		width: 600,
		height: 490,
		menu: {
			'File': [ 'Open', 'Exit' ],
			'Control': [ 'Full screen', 'Toggle sound', '-', 'Joystick on', 'Joystick off' ],
			'Download': [ 'c64.com', 'c64g.com' ],
			'Help': [ 'Help', 'About' ]
		},
		menuCallback: c64_menu_click
	});

	var c64 = c64_window.find('iframe')[0].contentWindow;
	var buttons = c64_window.find('div.keys button');
	buttons.addClass('btn btn-default btn-sm');
	buttons.mousedown(function() {
		var key = $(this).attr('key')
		c64.key_down(key);
	});
	buttons.mouseup(function() {
		var key = $(this).attr('key')
		c64.key_up(key);
		c64.focus();
	});

	c64_window.open();

	if (filename != undefined) {
		setTimeout(function() {
			orb_file_open(filename, function(content) {
				c64.open_file(content);
			});
		}, 5000);
	}
}

function c64_menu_click(c64_window, item) {
	var c64 = c64_window.find('iframe')[0].contentWindow;

	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				if (c64_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					orb_file_open(filename, function(content) {
						c64.open_file(content);
					});
				}
			}, 'Emulators/C64');
			break;
		case 'Exit':
			c64_window.close();
			break;
		case 'Full screen':
			c64.Module.requestC64FullScreen();
			break;
		case 'Toggle sound':
			c64.toggle_sound();
			break;
		case 'Joystick on':
			c64.joystick_on();
			break;
		case 'Joystick off':
			c64.joystick_off();
			break;
		case 'c64.com':
			window.open('https://www.c64.com/', '_blank');
			break;
		case 'c64g.com':
			window.open('https://c64g.com/', '_blank');
			break;
		case 'Help':
			orb_alert('Joystick keys: I, J, K, L and spacebar', 'Help');
			break;
		case 'About':
			orb_alert('C64 Javascript Emulator\nCopyright (c) by Thomas Hochgoetz\n<a href="https://c64emulator.111mb.de/" target="_blank">https://c64emulator.111mb.de/</a>', 'About');
			break;
	}
}

$(document).ready(function() {
	var icon = '/apps/c64/c64.png';
	orb_startmenu_add('C64', icon, c64_open);

	c64_valid_extensions.forEach(function(extension) {
		orb_upon_file_open(extension, c64_open, icon);
	});
});
