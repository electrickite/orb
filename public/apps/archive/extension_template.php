<?php
	namespace Orb;

	class EXT extends archiver_library implements archiver_interface {
		private $archive = null;
		private $EXT = null;

		public function open($archive) {
			$EXT = new \EXTENSION_HANDLER;
			if ($EXT->open($archive) !== true) {
				return false;
			}

			$this->archive = $archive;
			$this->EXT = $EXT;

			return true;
		}

		public function list() {
			if ($this->EXT === null) {
				return false;
			}

			$list = array();
			// Read files list from archive

			return $list;
		}

		public function extract($filename, $directory) {
			if ($this->EXT === null) {
				return false;
			}

			$target = $directory."/".basename($filename);
			// Read $filename from archive to $target

			return $target;
		}
	}
?>
