/* Orb Manual application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function manual_menu_click(manual_window, item) {
	switch (item) {
		case 'Example application':
			manual_load_file(manual_window, 'example.html');
			break;
		case 'Main page':
			manual_load_file(manual_window, 'main.html');
			break;
		case 'Orb_window class':
			manual_load_file(manual_window, 'window.html');
			break;
		case 'Frontend API':
			manual_load_file(manual_window, 'frontend.html');
			break;
		case 'Backend calls':
			manual_load_file(manual_window, 'backend.html');
			break;
		case 'User scripts':
			manual_load_file(manual_window, 'scripts.html');
			break;
		case 'Exit':
			manual_window.close();
			break;
		case 'About':
			orb_alert('Orb developer manual\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function manual_load_file(manual_window, filename) {
	$.ajax('/apps/manual/' + filename).done(function(content) {
		content = $(content);
		content.find('a').each(function() {
			$(this).click(function() {
				manual_load_file(manual_window, $(this).attr('href'));
				return false;
			});
		});

		manual_window.empty();
		manual_window.scrollTop(0);
		manual_window.html(content);
	});
}

function manual_open(filename = undefined) {
	var window_content =
		'<div class="manual">' +
		'</div>';

	var manual_window = $(window_content).orb_window({
		header:'Orb developer manual',
		icon: '/apps/manual/manual.png',
		width: 800,
		height: 600,
		menu: {
			'Pages': [ 'Main page', 'Example application', 'Orb_window class', 'Frontend API', 'Backend calls', 'User scripts', '-', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: manual_menu_click
	});

	manual_load_file(manual_window, 'main.html');

	manual_window.open();
}

$(document).ready(function() {
	orb_startmenu_add('Manual', '/apps/manual/manual.png', manual_open);
});
